import * as React from "react";
import { useState, useEffect, useRef } from "react";
import CommentIcon from "@mui/icons-material/Comment";
import MusicVideoIcon from "@mui/icons-material/MusicVideo";
import { Autocomplete, TextField } from "@mui/material";
import { createTheme, ThemeProvider } from "@mui/material/styles";
import { appStore } from "../store/app.store";
import { formatToMinuteSecond } from "../functions/GeneralFunc";
import _ from "lodash";
import { createMarkup } from "../functions/WaveformFunc";
import DeleteIcon from '@mui/icons-material/Delete';

const theme = createTheme({
  palette: {
    gray: {
      main: "#938F99",
    },
  },
});

const licenseOptions = [
  { label: "copyright", value: 'copyright' },
  { label: "cc by", value: 'cc by' },
  { label: "cc by-nc", value: 'cc by-nc' },
  { label: "cc by-nd", value: 'cc by-nd' },
  { label: "cc by-nc-nd", value: 'cc by-nc-n' },
  { label: "cc sampling+", value: 'cc sampling+' },
  { label: "cc 0", value: 'cc 0' },
  { label: "public domain", value: 'public domain' },
];

export default function SpeechGrid({ item, index }) {
  const { listCutEditor, setListCutEditor, cutEditorRegionSelected, setCutEditorRegionSelected } = appStore()
  const [startTime, setStartTime] = useState(null);
  const [endTime, setEndTime] = useState(null);
  const autoScrollRef = useRef(null);

  useEffect(() => {
    setStartTime(formatToMinuteSecond(item.from / 1000 + 0.001));
    setEndTime(formatToMinuteSecond(item.to / 1000));
  }, [item]);
  const handleChange = (type, e) => {
    item[type] = e.target.value
    setListCutEditor([...listCutEditor])
  }
  const handleLicense = (e, newValue) => {
    item.license = newValue.value
    setListCutEditor([...listCutEditor])
  }

  useEffect(() => {
    if (cutEditorRegionSelected) {
      if (cutEditorRegionSelected.content.id == index)
        autoScrollRef.current?.scrollIntoView({ inline: "nearest", block: "end" });
    }
  }, [cutEditorRegionSelected]);
  const handleClickSpan = () => {
    let all = window.wsRegions.getRegions();
    let idx = _.findIndex(all, v => { return v.content.id == index })
    if (idx >= 0) {
      _.forEach(all, (v) => {
        v.setOptions({
          id:
            v.content.attributes.type === "music"
              ? "cba-waveform-marker-music"
              : "cba-waveform-marker-spoken-word",
        });
      });

      let region = all[idx]
      region.setOptions({
        id: region.content.attributes.type === "music"
          ? "cba-waveform-marker-selected-music"
          : "cba-waveform-marker-selected-spoken-word"
      });
      setCutEditorRegionSelected(region)
    }
  }
  const handleDeleteRegion = () => {
    setCutEditorRegionSelected()
    let all = window.wsRegions.getRegions();
    _.forEach(all, (v) => {
      v.setOptions({
        id:
          v.content.attributes.type === "music"
            ? "cba-waveform-marker-music"
            : "cba-waveform-marker-spoken-word",
      });
    });
    let clone = [...listCutEditor]
    clone.splice(index, 1)
    setListCutEditor(clone)
    createMarkup(window.wsRegions, clone);

  }
  return (
    <ThemeProvider theme={theme}>
      <div style={{ display: "flex" }} className={`cba-speech-grid ${cutEditorRegionSelected?.content.id == index ? 'active' : ''}`} ref={autoScrollRef}>
        <div
          style={{
            width: 50,
            color: item.type !== "music" ? "#6AA84F" : "#CC0000",
          }}
        >
          {item.type === "music" ? <MusicVideoIcon /> : <CommentIcon />}
        </div>
        <div
          className="content"
          style={{
            padding: '20px 10px 20px 0px',
            width: "calc(100% - 50px)",
          
          }}
        >
          <div style={{ display: "flex", gap: 50,    borderLeft:
              item.type !== "music" ? "5px solid #6AA84F" : "5px solid #CC0000",}}>
            <div
              style={{
                width: 60,
                maxWidth: 60,
                minWidth: 60,
                color: "#E6E0E9",
                display:'flex',
                flexDirection:'column',
                gap:5,
               paddingLeft:10
              }}
            >
              <span>{startTime}</span>
              <span>{endTime}</span>
            </div>

            <div
              style={{
                display: "flex",
                flexDirection: "column",
                gap: 25,
                width: "100%",
              }}
            >
              <div style={{ display: 'flex', justifyContent: 'space-between', alignItems: 'center' }}>
                <span id="cba-span" onClick={handleClickSpan} >
                  {item.type !== "music" ? "Spoken word" : "Music"}
                </span>
                <div style={{ marginLeft: 50, cursor:'pointer' }} onClick={handleDeleteRegion}>
                  <DeleteIcon sx={{ color: "#CAC4D0 !important", fontSize: 22 }}
                  />
                </div>
              </div>
              <div
                style={{
                  display: "flex",
                  gap: 10,
                  justifyContent: "space-between",
                }}
              >
                <TextField
                  className="cba-text-field"
                  id="outlined-basic"
                  label="Title"
                  variant="outlined"
                  size="small"
                  sx={{
                    "& .MuiInputBase-root": {
                      color: "gray.main",
                    },
                  }}
                  color="gray"
                  value={item.title}
                  onChange={handleChange.bind(this,'title')}
                />
                <TextField
                  className="cba-text-field"
                  id="outlined-basic"
                  label="Author(s)"
                  variant="outlined"
                  size="small"
                  sx={{
                    "& .MuiInputBase-root": {
                      color: "gray.main",
                    },
                  }}
                  color="gray"
                  onChange={handleChange.bind(this,'author')}
                  value={item.author}
                />
                <Autocomplete
                  className="cba-text-field"
                  onChange={handleLicense}
                  options={licenseOptions}
                  value={item.license ?? "cc by"}
                  disableClearable="true"
                  renderInput={(params) => (
                    <TextField {...params} label="License" />
                  )}
                  size="small"
                  sx={{
                    "& .MuiInputBase-root": {
                      color: "gray.main",
                      width: 250,
                    },
                  }}
                  color="gray"
                />
                <TextField
                  className="cba-text-field"
                  id="outlined-basic"
                  label="Link"
                  variant="outlined"
                  size="small"
                  sx={{
                    "& .MuiInputBase-root": {
                      color: "gray.main",
                    },
                  }}
                  color="gray"
                  onChange={handleChange.bind(this,'link')}
                  value={item?.link}
                />
              </div>
            </div>
          </div>
        </div>
      </div>
    </ThemeProvider>
  );
}
