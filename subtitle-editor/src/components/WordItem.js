import * as React from "react";
import _ from "lodash";
import WordEdit from "./WordEdit";

const WordItem = ({ words, segementIndex }) => {
  return (
    <div style={{ display: "flex", gap: 5, flexWrap: "wrap" }}>
      {words.map((word, k) => (
        <div style={{ display: "flex", gap: 5, flexDirection: "column" }}>
          <div>
            <span style={{ fontWeight: "bold" }}>{"Time: "}</span>
            <span>{word.start}</span>
            <span>{` -> `}</span>
            <span>{word.end}</span>
          </div>
          <div>
            <span style={{ fontWeight: "bold" }}>{"Confidence: "}</span>
            <span>{word.confidence}</span>{" "}
          </div>
          <WordEdit word={word} segementIndex={segementIndex} wordIndex={k} />
        </div>
      ))}
    </div>
  );
};

export default WordItem;
