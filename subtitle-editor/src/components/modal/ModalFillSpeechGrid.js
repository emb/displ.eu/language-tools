import * as React from "react";
import Button from "@mui/material/Button";
import Dialog from "@mui/material/Dialog";
import DialogActions from "@mui/material/DialogActions";
import DialogContent from "@mui/material/DialogContent";
import DialogTitle from "@mui/material/DialogTitle";
import DialogContentText from '@mui/material/DialogContentText';
import { appStore } from "../../store/app.store";
import Select from "@mui/material/Select";
import MenuItem from "@mui/material/MenuItem";
import _ from "lodash";
import { createMarkup, exportCutEditor } from "../../functions/WaveformFunc";
import { useSearchParams } from "react-router-dom";

export default function ModalFillSpeechGrid() {
  const { setListCutEditor, listCutEditor, setIsFillNewSpeechGrid, setIsLoading, duration, cutEditorFile } =
    appStore();
  const [type, setType] = React.useState("spokenword");
  const [search] = useSearchParams();
  const post_id = search.get("post_id");
  const handleClose = () => {
    setIsFillNewSpeechGrid(false);
  };
  const handleChange = (event) => {
    setType(event.target.value);
  };

  const handleOk = async () => {
    setIsLoading(true)
    try {
      // window.wsRegions.clear
      let clone = [];
      if (listCutEditor.length === 0) {
        clone.push({
          author: "Unknown author",
          from: 0,
          license: type === "music"
            ? "copyright"
            : "cc by",
          title: "Unknown title",
          to: duration * 1000,
          type: type,
        });
      } else if (listCutEditor.length === 1) {
        let current = listCutEditor[0];
        if (current.from < 500) {
          current.from = 0
        } else {
          clone.push({
            author: "Unknown author",
            from: 0,
            license: type === "music"
              ? "copyright"
              : "cc by",
            title: "Unknown title",
            to: current.from,
            type: type,
          });
        }
        clone.push(current)
        if (duration * 1000 - current.to > 500) {
          clone.push({
            author: "Unknown author",
            from: current.to,
            license: type === "music"
              ? "copyright"
              : "cc by",
            title: "Unknown title",
            to: duration * 1000,
            type: type,
          });
        }
        current.to = duration * 1000
      } else {
        for (let i = 0; i < listCutEditor.length; i++) {
          let current = listCutEditor[i];
          if (i === 0) {
            if (current.from < 500) {
              current.from = 0

            } else {
              clone.push({
                author: "Unknown author",
                from: 0,
                license: type === "music"
                  ? "copyright"
                  : "cc by",
                title: "Unknown title",
                to: current.from,
                type: type,
              });
            }
            clone.push(current)
          } else if (i !== listCutEditor.length - 1) {
            let previous = listCutEditor[i - 1];
            let diff = previous.to - current.from;
            if (diff) {
              clone.push({
                author: "Unknown author",
                from: previous.to,
                license: type === "music"
                  ? "copyright"
                  : "cc by",
                title: "Unknown title",
                to: current.from,
                type: type,
              });
            }
            clone.push(current)
          }

          else if (i === listCutEditor.length - 1) {
            let previous = listCutEditor[i - 1];
            let diff = previous.to - current.from;
            if (diff) {
              clone.push({
                author: "Unknown author",
                from: previous.to,
                license: type === "music"
                  ? "copyright"
                  : "cc by",
                title: "Unknown title",
                to: current.from,
                type: type,
              });
            }
   

            if (duration * 1000 - current.to < 500) {
              current.to = duration * 1000
            }
            clone.push(current)
            if (duration * 1000 - current.to >= 500) {
              clone.push({
                author: "Unknown author",
                from: current.to,
                license: type === "music"
                  ? "copyright"
                  : "cc by",
                title: "Unknown title",
                to: duration * 1000,
                type: type,
              });
            }

          }
        }
      }
      debugger
      setListCutEditor(clone);
      createMarkup(window.wsRegions, clone);
      await exportCutEditor(post_id, cutEditorFile, clone, true)
      setIsLoading(false)
      handleClose();
    } catch (e) {
      alert(e.message)
      setIsLoading(false)
    }

  };
  return (
    <React.Fragment>
      <Dialog open={true} onClose={handleClose}>
        <DialogTitle>Fill gaps</DialogTitle>
        <DialogContent>
          <DialogContentText >
            Select a type to describe the gaps
          </DialogContentText>
          <p />
          <div style={{ display: "flex", gap: 5 }}>
            <Select
              labelId="demo-simple-select-label"
              value={type}
              // label="Type"
              onChange={handleChange}
              fullWidth
            >
              <MenuItem value={"spokenword"}>Spoken word</MenuItem>
              <MenuItem value={"music"}>Music</MenuItem>
            </Select>
          </div>
        </DialogContent>
        <DialogActions>
          <Button onClick={handleClose}>Cancel</Button>
          <Button onClick={handleOk}>OK</Button>
        </DialogActions>
      </Dialog>
    </React.Fragment>
  );
}
