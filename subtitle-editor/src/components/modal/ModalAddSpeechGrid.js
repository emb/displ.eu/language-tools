import * as React from "react";
import Button from "@mui/material/Button";
import Dialog from "@mui/material/Dialog";
import DialogActions from "@mui/material/DialogActions";
import DialogContent from "@mui/material/DialogContent";
import DialogTitle from "@mui/material/DialogTitle";
import { appStore } from "../../store/app.store";
import Select from "@mui/material/Select";
import MenuItem from "@mui/material/MenuItem";
import _ from "lodash";
import { createMarkup, snapSpeechGrid } from "../../functions/WaveformFunc";
export default function ModalAddSpeechGrid() {
  const {
    setListCutEditor,
    listCutEditor,
    setIsAddNewSpeechGrid,
    newSpeechGrid,
  } = appStore();
  const [type, setType] = React.useState("spokenword");
  const [start, setStart] = React.useState({
    hour: 0,
    minute: 0,
    second: 0,
  });
  const [end, setEnd] = React.useState({
    hour: 0,
    minute: 0,
    second: 0,
  });
  const handleClose = () => {

      newSpeechGrid.remove()
    setIsAddNewSpeechGrid(false);
  };
  const handleChange = (event) => {
    setType(event.target.value);
  };

  const handleOk = () => {
    let clone = [...listCutEditor];
    clone.push({
      author: "Unknown author",
      from: newSpeechGrid.start * 1000,
      license: type == 'music' ? "copyright" : "cc by",
      title: "Unknown title",
      to: newSpeechGrid.end * 1000,
      type: type,
      isUserDefined: true
    });
    clone.sort((a, b) => {
      if (a.from + a.to > b.from + b.to) return 1;
      else return -1;
    });
    let index = _.findIndex(clone, (v) => {
      return v.isUserDefined;
    });
    //* Remove temporary field
    delete clone[index].isUserDefined;
 

    let current = clone[index]
    let next = null
    let previous = null
    if (index === 0) {
      next = clone[index + 1]
    } else if (index === clone.length - 1) {
      previous = clone[index - 1]
    } else {
      next = clone[index + 1]
      previous = clone[index - 1]
    }

    const { from, to }= snapSpeechGrid(current, next, previous, 0.2, 1000)
    clone[index].from = from
    clone[index].to = to
    setListCutEditor(clone);
    createMarkup(window.wsRegions, clone)
    setIsAddNewSpeechGrid(false);
  };
  return (
    <React.Fragment>
      <Dialog open={true} onClose={handleClose}>
        <DialogTitle>Fill region as</DialogTitle>
        <DialogContent>
          <div style={{ display: "flex", gap: 5 }}>
            <Select
              labelId="demo-simple-select-label"
              id="demo-simple-select"
              value={type}
              label="Type"
              onChange={handleChange}
            >
              <MenuItem value={"spokenword"}>Spoken word</MenuItem>
              <MenuItem value={"music"}>Music</MenuItem>
            </Select>
          </div>
        </DialogContent>
        <DialogActions>
          <Button onClick={handleClose}>Cancel</Button>
          <Button onClick={handleOk}>OK</Button>
        </DialogActions>
      </Dialog>
    </React.Fragment>
  );
}
