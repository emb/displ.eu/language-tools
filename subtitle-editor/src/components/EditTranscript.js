import { TextField } from "@mui/material";
import * as React from "react";
import { useState, useEffect, useRef } from "react";
import { appStore } from "../store/app.store";
import { createTheme, ThemeProvider } from "@mui/material/styles";
import { formatToMinuteSecond } from "../functions/GeneralFunc";

const theme = createTheme({
  palette: {
    lightPurple: {
      main: "#EADDFF",
      contrastText: "#E6E0E9",
    },
  },
});

const EditTranscript = ({ item, text, type }) => {
  const {
    currentTime,
    setCurrentTranscriptSelected,
    currentTranscriptSelected,
    waveform,
  } = appStore();
  const [content, setContent] = useState(text);
  const [isCurrent, setIsCurrent] = useState();
  const [startTime, setStartTime] = useState(null);
  const [endTime, setEndTime] = useState(null);
  const autoScrollRef = useRef(null);
  useEffect(() => {
    setContent(text)
    setStartTime(formatToMinuteSecond(item.start));
    setEndTime(formatToMinuteSecond(item.end));
  }, [item])
  React.useEffect(() => {
    setIsCurrent(false);
    debugger
    if (item.start <= currentTime) {
      if (item.end > currentTime) {
        setIsCurrent(true);
      }
    }
  }, [currentTime]);
  const handleDoubleClick = (e) => {
    e.preventDefault();
    e.stopPropagation();
    console.log(currentTranscriptSelected);
    setCurrentTranscriptSelected({ id: item.id, type });
  };
  const handleChangeContent = (event) => {
    setContent(event.target.value);
  };
  const handleStopEdit = () => {
    setCurrentTranscriptSelected();
  };
  const handleCorrespondingToWaveform = (e) => {
    e.preventDefault();
    e.stopPropagation();
    waveform.setTime(item.start);

  };


  useEffect(() => {
    autoScrollRef.current?.scrollIntoView({
      behavior: 'auto',
      block: 'center',
      inline: 'center'
    });
  }, [isCurrent]);

  return (
    <ThemeProvider theme={theme}>
      <div
        className={`${type}-container`}
        style={{
          display: "flex",
          paddingLeft: 5,
        }}
        onClick={handleCorrespondingToWaveform}
        ref={autoScrollRef}
      >
        <div
          style={{
            display: "flex",
            flexDirection: "column",
            width: 100,
            cursor: "pointer",
            justifyContent: "center",
          }}
          onDoubleClick={handleStopEdit}
        >
          <div
            style={{
              display: "flex",
              flexDirection: "column",
              borderLeft: isCurrent && "4px solid #EADDFF",
              gap: 2,
              paddingLeft: 5,
            }}
          >
            <span data-time={item.start} >{startTime}</span>
            <span data-time={item.end}>{endTime}</span>
          </div>
        </div>

        {currentTranscriptSelected?.id === item.id &&
          currentTranscriptSelected?.type === type ? (
          <TextField
            className="transcript-edit"
            multiline
            maxRows={4}
            value={content}
            style={{ width: "100%" }}
            onChange={handleChangeContent}
            color="lightPurple"
            // focused
            sx={{
              "& .MuiInputBase-root": {
                color: "lightPurple.contrastText",
              },
            }}
            focused
          />
        ) : (
          <div
            onDoubleClick={handleDoubleClick}
            style={{ width: "100%", cursor: "pointer" }}
            className="transcript-edit"
          >
            {content}
          </div>
        )}
        {/* <div
     
          id="testing"
          style={{ position: "relative", top: 50 }}
        /> */}
      </div>
    </ThemeProvider>
  );
};

export default EditTranscript;
