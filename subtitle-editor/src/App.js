import { Route, Routes } from "react-router-dom";
import HomePageTranscriptEditor from "./pages/HomePageTranscriptEditor";
import HomePageCutEditor from "./pages/HomePageCutEditor";
import NoPage from "./pages/NoPage";
import AuthRoute from "./routes/AuthRoute";

function App() {
  return (
    <Routes>
      <Route element={<AuthRoute />}>
        <Route path="/" element={<HomePageCutEditor />} />
        <Route
          path="/transcript-editor"
          element={<HomePageTranscriptEditor />}
        />
      </Route>
      <Route path="*" element={<NoPage />} />
    </Routes>
  );
}

export default App;