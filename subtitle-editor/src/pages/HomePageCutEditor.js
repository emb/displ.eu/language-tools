import CutEditorContainerProperties from "../layout/homepage/CutEditorContainerProperties";
import WaveFormCutAudio from "../layout/homepage/WaveFormCutAudio";
import CutEditorActionBar from "../layout/homepage/CutEditorActionBar";
import MusicSpeechSection from "../layout/homepage/MusicSpeechSection";
import { appStore } from "../store/app.store";
import ModalAddSpeechGrid from "../components/modal/ModalAddSpeechGrid";
import { Backdrop, CircularProgress } from "@mui/material";
import ModalFillSpeechGrid from "../components/modal/ModalFillSpeechGrid";
import { useSearchParams } from "react-router-dom";
import axios from "axios";
import _ from "lodash";
import { useEffect } from "react";

const HomePage = () => {
  const {
    fileUrlCutEditor,
    listCutEditor,
    isFillNewSpeechGrid,
    isLoading,
    isAddNewSpeechGrid,
    newSpeechGrid,
    setIsLoading,
    setListCutEditor,
    setFileUrlCutEditor,
    setCutEditorFile,
    setIsPlay,
    isPlay
  } = appStore();

  const [search] = useSearchParams();
  const post_id = search.get('post_id');
  useEffect(() => {
    window.addEventListener('keydown', onWaveformPress)
    return () => {
      window.removeEventListener('keydown', onWaveformPress)
    }
  }, [isPlay])

  const onWaveformPress = (e) => {
    if (e.code === 'Space') {
      setIsPlay(!isPlay)
    }
  }

  useEffect(() => {
    if (!post_id) {
      alert("Missing ID");
      return;
    }
    setIsLoading(true);
    axios
      .get(
        `https://cba.media/wp-admin/admin-ajax.php`, {
        params: {
          post_id,
          action: 'trp_editor_load_regions',
        },
      }
      )
      .then((res) => {
        console.log(res.data);

        setCutEditorFile(res.data.file)
        setListCutEditor(res.data.regions);
        setFileUrlCutEditor(res.data.file.url);
      })
      .catch((ex) => {
        alert(ex.message);
        setIsLoading(false);
      });
  }, [post_id]);

  return (
    <div
      style={{
        display: "flex",
        width: "100%",
        height: "100%",
        backgroundColor: "#1D1B20",
      }}
    >
      <Backdrop
        sx={{ color: "#fff", zIndex: (theme) => theme.zIndex.drawer + 1, backgroundColor: "rgba(9, 9, 9, 0.75)" }}
        open={isLoading}
      >
        <div style={{display:'flex', flexDirection:'column', gap:5, alignItems:'center'}} >  
          <CircularProgress color="inherit" />
          <span>
            {"Please wait. The file may take some time to load... "}
          </span>
        </div>


      </Backdrop>
      <CutEditorContainerProperties />
      <div style={{ width: "calc(100% - 500px)", height: "100%", overflow: 'auto' }}>
        {fileUrlCutEditor && listCutEditor && <WaveFormCutAudio />}
        <CutEditorActionBar />
        <MusicSpeechSection />
      </div>
      {isAddNewSpeechGrid && newSpeechGrid && <ModalAddSpeechGrid />}
      {isFillNewSpeechGrid && <ModalFillSpeechGrid />}
    </div>
  );
};

export default HomePage;
