import { useEffect } from "react";
import WaveSurfer from "wavesurfer.js";
import Minimap from "wavesurfer.js/dist/plugins/minimap.esm.js";
import Timeline from "wavesurfer.js/dist/plugins/timeline.esm.js";
import RegionsPlugin from "wavesurfer.js/dist/plugins/regions.esm.js";
import ToolbarWaveform from "./ToolbarWaveform";
import * as React from "react";
import Stack from "@mui/material/Stack";
import { appStore } from "../../store/app.store";
import _ from "lodash";
import { createMarkup, snapSpeechGrid } from "../../functions/WaveformFunc";

window.wsRegions = null;

let durationLocal = 0
const WaveFormAudio = ({ data1 }) => {
  const {
    setCurrentTime,
    setWaveform,
    setIsPlay,
    setDuration,
    setIsLoading,
    listCutEditor,
    fileUrlCutEditor,
    setListCutEditor,
    setNewSpeechGrid,
    setIsAddNewSpeechGrid,
    setCutEditorRegionSelected
  } = appStore();
  const [isReady, setIsReady] = React.useState();

  useEffect(() => {
    durationLocal = 0
    window.wsRegions = RegionsPlugin.create();
    const ws = WaveSurfer.create({
      container: "#waveform-container",
      waveColor: "#8981F8",
      progressColor: "#8981F8",
      cursorColor: '#FF5400',
      cursorWidth: 2,
      url: fileUrlCutEditor,
      height: 220,
      width: "100%",
      barWidth: 4,
      barGap: 2,
      barRadius: 2,
      minPxPerSec: 100,
      hideScrollbar: false,
      autoCenter: false,
      plugins: [
        Timeline.create({
          height: 25,
          insertPosition: 'beforebegin',
          style: {
            fontSize: "10px",
            color: "#E6E0E9",
            padding: 200,
          },
        }),
        Minimap.create({
          height: 50,
          width: "100%",
          waveColor: "#8981F8",
          progressColor: "#8981F8",
          barWidth: 2,
          barGap: 1,
          barRadius: 1,
        }),
        window.wsRegions,

      ],
    });

    window.wsRegions.enableDragSelection({ color: "#bcbcbc80" });

    ws.on("timeupdate", timeupdate);
    ws.on("finish", finish);
    ws.on("ready", ready);
    setWaveform(ws);
    return () => {
      window.wsRegions.un("region-updated", updateRegion);
      window.wsRegions.un("region-created", createRegion);
      ws.un("timeupdate", timeupdate);
      ws.un("finish", finish);
      ws.destroy();
      window.wsRegions.destroy();
      setWaveform(null);
    };
  }, []);
  useEffect(() => {
    if (isReady) {
      let result = [...listCutEditor]
      setListCutEditor(result)
      createMarkup(window.wsRegions, result);
      setIsLoading(false)
    }
  }, [isReady]);

  useEffect(() => {
    window.wsRegions.on("region-clicked", clickRegion);
    window.wsRegions.on("region-updated", updateRegion);
    window.wsRegions.on("region-created", createRegion);
    return () => {
      if (window.wsRegions) {
        window.wsRegions?.un("region-updated", updateRegion);
        window.wsRegions?.un("region-created", createRegion);
        window.wsRegions?.un("region-clicked", clickRegion);
      }
    };
  }, [listCutEditor]);

  const clickRegion = (region,e) => {
    // e.stopPropagation() //TODO
    if (region.id.includes('cba-waveform-marker-selected')) {
      region.setOptions({
        id:
          region.content.attributes.type === "music"
            ? "cba-waveform-marker-music"
            : "cba-waveform-marker-spoken-word",
      });
      setCutEditorRegionSelected()
    } else {
      let all = window.wsRegions.getRegions();
      _.forEach(all, (v) => {
        v.setOptions({
          id:
            v.content.attributes.type === "music"
              ? "cba-waveform-marker-music"
              : "cba-waveform-marker-spoken-word",
        });
      });
      region.setOptions({
        id:  region.content.attributes.type === "music"
        ? "cba-waveform-marker-selected-music"
        : "cba-waveform-marker-selected-spoken-word"
      });
      setCutEditorRegionSelected(region)
    }
  }
  const createRegion = (region) => {
    console.log(region);
    if (!region.content) {
      try {
        let all = window.wsRegions.getRegions();
        let isOverlap = false;
        _.forEach(all, (v) => {
          if (v.content) {
            if (v.start <= region.start && v.end >= region.start) {
              isOverlap = true;
              return false;
            }
            if (v.start <= region.end && v.end >= region.end) {
              isOverlap = true;
              return false;
            }
            if (v.start >= region.start && v.end <= region.end) {
              isOverlap = true;
              return false;
            }
          }
        });
        if (isOverlap) region?.remove();
        else {
          setNewSpeechGrid(region)
          setIsAddNewSpeechGrid(true)
        }
      } catch { }
    }
  };
  const updateRegion = (region) => {
    console.log("Updated region", region);
    let all = window.wsRegions.getRegions();
    let previous = null;
    let next = null;
    if (region.content.id * 1 === 0) {
      next = all[region.content.id * 1 + 1];
    } else if (region.content.id * 1 === all.length - 1) {
      previous = all[region.content.id * 1 - 1];
    } else {
      previous = all[region.content.id * 1 - 1];
      next = all[region.content.id * 1 + 1];
    }
    let isOverlap = false
    let start = region.start;
    let end = region.end;
    if (previous) {
      if (previous.end > region.start) {
        start = previous.end;
        isOverlap = true
      }
    }
    if (next) {
      if (next.start < region.end) {
        end = next.start;
        isOverlap = true
      }
    }

    if (!isOverlap) {
      const { from, to } = snapSpeechGrid({ from: region.start, to: region.end },
        next ? { from: next.start, to: next.end } : null,
        previous ? { from: previous.start, to: previous.end } : null, 0.2, 1)
      start = from
      end = to
    }

    region.setOptions({
      start,
      end,
    });

    let clone = JSON.parse(JSON.stringify(listCutEditor));
    clone[region.content.id * 1].from = start * 1000;
    clone[region.content.id * 1].to = end * 1000;
    setListCutEditor(clone);
  };

  const timeupdate = (currentTime) => {
    setCurrentTime(currentTime);
  };
  const finish = () => {
    setIsPlay(false);
  };
  const ready = (duration) => {
    durationLocal = duration
    setDuration(duration);
    setIsLoading(false);
    setIsReady(true);
  };
  return (
    <>
      <div style={{ width: "100%", backgroundColor: "#1D1B20"}}>
      <div
              id="waveform-container"
              style={{
                width: "100%",
              }}
            ></div>
      </div>
    </>
  );
};

export default WaveFormAudio;
