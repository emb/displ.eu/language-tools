import { useEffect } from "react";
import WaveSurfer from "wavesurfer.js";
import Minimap from "wavesurfer.js/dist/plugins/minimap.esm.js";
import Timeline from "wavesurfer.js/dist/plugins/timeline.esm.js";
import ToolbarWaveform from "./ToolbarWaveform";
import * as React from "react";
import Stack from "@mui/material/Stack";
import { appStore } from "../../store/app.store";

const WaveFormAudio = ({ data1 }) => {
  const {
    setCurrentTime,
    setWaveform,
    setIsPlay,
    setDuration,
    setIsLoading,
    fileUrl,
  } = appStore();

  useEffect(() => {
    const ws = WaveSurfer.create({
      container: "#waveform-container",
      waveColor: "#8981F8",
      progressColor: "#8981F8",
      cursorColor: '#FF5400',
      cursorWidth: 2,
      url: fileUrl,
      height: 245,
      width: "100%",
      barWidth: 4,
      barGap: 2,
      barRadius: 2,
      minPxPerSec: 100,
      hideScrollbar: true,
      autoCenter: false,
      plugins: [
        Minimap.create({
          height: 50,
          width: "100%",
          waveColor: "#8981F8",
          progressColor: "#8981F8",
          barWidth: 2,
          barGap: 1,
          barRadius: 1,
        }),
        Timeline.create({
          height: 25,
          insertPosition: "beforebegin",
          style: {
            fontSize: "10px",
            color: "#E6E0E9",
            padding: 200,
          },
        }),
      ],
    });
    ws.on("timeupdate", timeupdate);
    ws.on("finish", finish);
    ws.on("ready", ready);
    setWaveform(ws);

    return () => {
      ws.un("timeupdate", timeupdate);
      ws.un("finish", finish);
      ws.destroy();
      setWaveform(null);
    };
  }, []);

  const timeupdate = (currentTime) => {
    setCurrentTime(currentTime);
  };
  const finish = () => {
    setIsPlay(false);
  };
  const ready = (duration) => {
    setDuration(duration);
    setIsLoading(false);
  };
  return (
    <>
      <div style={{ width: "100%", backgroundColor: "#1D1B20", height: 295 }}>
        <Stack direction="row" spacing={1} width={"100%"} height={"100%"}>
          <ToolbarWaveform></ToolbarWaveform>
          <div style={{ width: "calc(100% - 30px)" }}>
            <div
              id="waveform-container"
              style={{
                width: "100%",
              }}
            ></div>
          </div>
        </Stack>
      </div>
    </>
  );
};

export default WaveFormAudio;
