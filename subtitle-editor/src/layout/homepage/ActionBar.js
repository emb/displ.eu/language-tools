import * as React from "react";
import { useEffect } from "react";
import Slider from "@mui/material/Slider";
import ZoomOutIcon from "@mui/icons-material/ZoomOut";
import ZoomInIcon from "@mui/icons-material/ZoomIn";
import ButtonGroup from "@mui/material/ButtonGroup";
import IconButton from "@mui/material/IconButton";
import FastRewindRoundedIcon from "@mui/icons-material/FastRewindRounded";
import FastForwardRoundedIcon from "@mui/icons-material/FastForwardRounded";
import Button from "@mui/material/Button";
import { createTheme, ThemeProvider } from "@mui/material/styles";
import { appStore } from "../../store/app.store";
import { KeyboardArrowDown, PauseRounded, PlayArrowRounded } from "@mui/icons-material";
import { saveAs } from "file-saver";
import _ from "lodash";
import { useSearchParams } from "react-router-dom";
import { formatToMinuteSecond, sleep } from "../../functions/GeneralFunc";
import Menu from "@mui/material/Menu";
import MenuItem from "@mui/material/MenuItem";

let theme = createTheme({});
theme = createTheme(theme, {
  palette: {
    violet: theme.palette.augmentColor({
      color: {
        main: "#D0BCFF",
        contrastText: "#381E72",
      },
      name: "violet",
    }),
  },
});

const ActionBar = () => {
  const {
    currentTime,
    waveform,
    isPlay,
    setIsPlay,
    duration,
    currentTranscriptSelected,
    transcriptFile,
    setIsLoading,
    setCurrentTranscriptSelected
  } = appStore();
  const [value, setValue] = React.useState(0);
  const [formattedCurrentTime, setFormattedCurrentTime] = React.useState(null);
  const [formattedDuration, setFormattedDuration] = React.useState(null);
  const [search] = useSearchParams();
  const transcript_id = search.get("transcript_id");
  const [anchorEl, setAnchorEl] = React.useState(null);
  const open = Boolean(anchorEl);
  const handleClick = (event) => {
    setAnchorEl(event.currentTarget);
  };
  const handleClose = () => {
    setAnchorEl(null);
  };

  React.useEffect(() => {
    if (waveform) {
      if (isPlay) {
        waveform.play();
      } else {
        waveform.pause();
      }
    }
  }, [isPlay, waveform]);
  const handleChange = (event, newValue) => {
    setValue(newValue);
  };
  const handlePlay = () => {
    setIsPlay(!isPlay);
  };
  React.useEffect(() => {
    if (duration > 0) {
      if (waveform) {
        waveform.zoom(value);
      }
    }
  }, [value, waveform, duration]);

  const handleForwardSkip = () => {
    waveform.skip(10);
  };
  const handleBackwardSkip = () => {
    waveform.skip(-10);
  };

  useEffect(() => {
    setFormattedCurrentTime(formatToMinuteSecond(currentTime));
  }, [currentTime]);
  useEffect(() => {
    setFormattedDuration(formatToMinuteSecond(duration));
  }, [duration]);
  const exportTranscript = async () => {
    if (currentTranscriptSelected) {
      setCurrentTranscriptSelected()
      await sleep(1500)
    }
    if (!transcriptFile) {
      return;
    }
    let elements = document.getElementsByClassName("transcript-container");

    let contents = [];
    let text = "";
    _.forEach(elements, (node) => {
      let start =parseFloat(node.childNodes[0].childNodes[0].childNodes[0].dataset.time);
      let end = parseFloat(node.childNodes[0].childNodes[0].childNodes[1].dataset.time);
      let content = node.childNodes[1].innerText;
      contents.push({ start, end, content });
      text = text + ` ${content}`;
    });

    return { contents, text }
  }
  const handleExport = async () => {
    setIsLoading(true)
    debugger
    let { contents, text } = await exportTranscript()
    var data = new URLSearchParams();
    data.append("action", "trp_editor_save_transcript");
    data.append("transcript_id", parseInt(transcript_id));
    data.append("language", transcriptFile.transcript.language);
    data.append("transcript", JSON.stringify(text));
    data.append("segments", JSON.stringify(contents));
    data.append("_ajax_nonce", transcriptFile.nonce)
    console.log(contents)

    fetch("https://cba.media/wp-admin/admin-ajax.php", {
      method: "POST",
      body: data,
      headers: {
        "Content-type": "application/x-www-form-urlencoded; charset=UTF-8",
      },
    });
    setIsLoading(true)
  };
  const handleWebVTT = () => {
    window.open(transcriptFile.transcript.subtitles)
  }
  const handleSaveAsText = async () => {
    let { contents, text } = await exportTranscript()

    let clone = { ...transcriptFile };
    debugger
    clone.transcript.segments = contents;
    clone.transcript.text = text;

    var blob = new Blob([JSON.stringify(clone)], {
      type: "text/plain;charset=utf-8",
    });
    saveAs(blob, "transcript.json");
  };

  return (
    <ThemeProvider theme={theme}>
      {" "}
      <div
        style={{
          display: "flex",
          flexDirection: "column",
          height: 80,
          backgroundColor: "#202027",
          color: "#E6E0E9",
          padding: "0px 15px",
          border: "1px solid #322F35",
        }}
      >
        <div
          style={{
            display: "flex",
            justifyContent: "space-between",
            alignItems: "center",
            height: "100%",
          }}
        >
          <div
            style={{
              display: "flex",
              minWidth: 200,
              gap: 5,
              alignItems: "center",
            }}
          >
            <ZoomOutIcon />
            <Slider
              aria-label="Volume"
              value={value}
              onChange={handleChange}
              color="violet"
            />
            <ZoomInIcon style={{ paddingLeft: 9 }} />
          </div>
          <div style={{ display: "flex", alignItems: "center", gap: 10 }}>
            <div>
              <div>{formattedCurrentTime}</div>
            </div>
            <div>
              <ButtonGroup size="large">
                <IconButton
                  aria-label="rewind"
                  sx={{ color: "#D0BCFF" }}
                  onClick={handleBackwardSkip}
                >
                  <FastRewindRoundedIcon style={{ fontSize: 35 }} />
                </IconButton>
                <IconButton
                  aria-label="skip"
                  sx={{ color: "#D0BCFF" }}
                  onClick={handlePlay}
                >
                  {!isPlay ? (
                    <PlayArrowRounded style={{ fontSize: 35 }} />
                  ) : (
                    <PauseRounded style={{ fontSize: 35 }} />
                  )}
                </IconButton>
                <IconButton
                  aria-label="forward"
                  sx={{ color: "#D0BCFF" }}
                  onClick={handleForwardSkip}
                >
                  <FastForwardRoundedIcon style={{ fontSize: 35 }} />
                </IconButton>
              </ButtonGroup>
            </div>
            <div>{formattedDuration}</div>
          </div>
          <div style={{ display: "flex", gap: 10 }}>
            <Button
              variant="contained"
              color="violet"
              style={{ borderRadius: 100 }}
              onClick={handleExport}
            >
              Save
            </Button>
            <div>
              <Button
                id="basic-button"
                variant="contained"
                color="violet"
                style={{ borderRadius: 100 }}
                aria-controls={open ? "basic-menu" : undefined}
                aria-haspopup="true"
                aria-expanded={open ? "true" : undefined}
                onClick={handleClick}
                endIcon={<KeyboardArrowDown />}
              >
                Export
              </Button>
              <Menu
                id="basic-menu"
                anchorEl={anchorEl}
                open={open}
                onClose={handleClose}
                MenuListProps={{
                  "aria-labelledby": "basic-button",
                }}
              >
                <MenuItem onClick={handleWebVTT}>as WebVTT</MenuItem>
                <MenuItem onClick={handleSaveAsText}>as Text</MenuItem>
              </Menu>
            </div>
          </div>
        </div>
      </div>
    </ThemeProvider>
  );
};

export default ActionBar;
