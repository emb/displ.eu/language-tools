import * as React from "react";
import List from "@mui/material/List";
import ListItem from "@mui/material/ListItem";
import ListItemText from "@mui/material/ListItemText";
import ListItemButton from "@mui/material/ListItemButton";
import ListItemIcon from "@mui/material/ListItemIcon";
import Checkbox from "@mui/material/Checkbox";
import Divider from "@mui/material/Divider";
import Typography from "@mui/material/Typography";
import { createTheme, ThemeProvider } from "@mui/material/styles";
import axios from "axios";
import _ from "lodash";
import { appStore } from "../../store/app.store";
import { useSearchParams } from "react-router-dom";

let theme = createTheme({});
theme = createTheme(theme, {
  palette: {
    violet: theme.palette.augmentColor({
      color: {
        main: "#D0BCFF",
        contrastText: "#381E72",
      },
      name: "violet",
    }),
  },
});

export default function CutEditorProperties() {
  const [translations, setTranslations] = React.useState([]);
  const [transcripts, setTranscripts] = React.useState([]);

  const {
    setIsLoading,
    setTranslationContent,
  } = appStore();
  const [check, setCheck] = React.useState("");
  const [search] = useSearchParams();

  const handleCheck = async (item, e) => {
    debugger;
    setIsLoading(true);
    try {
      if (check == item.id) {
        if (!e.target.checked) {
          setCheck("");
          setTranslationContent(null);
          setIsLoading(false);
          return;
        }
      }
      let res = await axios.get(
        `https://cba.media/wp-admin/admin-ajax.php?transcript_id=${item.id}&action=trp_editor_load_transcript`
      );
      setTranslationContent(res.data.transcript);
      setCheck(item.id);
      console.log(res.data);
    } catch (ex) {
      alert(ex.message);
    }
    setIsLoading(false);
  };
  return (
    <>
      <ThemeProvider theme={theme}>
        <List
          sx={{
            width: "100%",
            maxWidth: 470,
            bgcolor: "#1D1B20",
            color: "#E6E0E9",
          }}
        >
          <ListItem disablePadding>
            <ListItemButton>
              <ListItemText primary="Transcription" />
            </ListItemButton>
          </ListItem>
          {transcripts.map((v) => (
            <ListItem>
              <ListItemButton role={undefined} dense>
                <ListItemIcon>
                  <Checkbox
                    edge="start"
                    tabIndex={-1}
                    disableRipple
                    color="violet"
                    checked={true}
                  />
                </ListItemIcon>
                <ListItemText
                  primary={v.language_string}
                  secondary={
                    <React.Fragment>
                      <Typography
                        sx={{ display: "inline" }}
                        component="span"
                        variant="body2"
                        color="#E6E0E9"
                      >
                        Date created: {v.date_created}, Author:{" "}
                        {v.author === "" ? "None" : v.author}
                      </Typography>
                    </React.Fragment>
                  }
                />
              </ListItemButton>
            </ListItem>
          ))}

          <Divider />

          <ListItem disablePadding>
            <ListItemButton>
              <ListItemText primary="Translation" />
            </ListItemButton>
          </ListItem>

          {translations.map((v) => (
            <ListItem>
              <ListItemIcon>
                <Checkbox
                  edge="start"
                  color="violet"
                  onChange={handleCheck.bind(this, v)}
                  checked={v.id === check}
                />
              </ListItemIcon>
              <ListItemText
                primary={v.language_string}
                secondary={
                  <React.Fragment>
                    <Typography
                      sx={{ display: "inline" }}
                      component="span"
                      variant="body2"
                      color="#E6E0E9"
                    >
                      Date created: {v.date_created}, Author:{" "}
                      {v.author === "" ? "None" : v.author}
                    </Typography>
                  </React.Fragment>
                }
              />
            </ListItem>
          ))}
        </List>
      </ThemeProvider>
    </>
  );
}