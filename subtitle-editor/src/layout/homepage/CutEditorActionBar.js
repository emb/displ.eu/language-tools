import * as React from "react";
import { useEffect } from "react";
import Slider from "@mui/material/Slider";
import ZoomOutIcon from "@mui/icons-material/ZoomOut";
import ZoomInIcon from "@mui/icons-material/ZoomIn";
import ButtonGroup from "@mui/material/ButtonGroup";
import IconButton from "@mui/material/IconButton";
import FastRewindRoundedIcon from "@mui/icons-material/FastRewindRounded";
import FastForwardRoundedIcon from "@mui/icons-material/FastForwardRounded";
import Button from "@mui/material/Button";
import { createTheme, ThemeProvider } from "@mui/material/styles";
import { appStore } from "../../store/app.store";
import { PauseRounded, PlayArrowRounded } from "@mui/icons-material";
import _ from "lodash";
import { formatToMinuteSecond } from "../../functions/GeneralFunc";
import { useSearchParams } from "react-router-dom";
import { createMarkup, exportCutEditor } from "../../functions/WaveformFunc";

let theme = createTheme({});
theme = createTheme(theme, {
  palette: {
    violet: theme.palette.augmentColor({
      color: {
        main: "#D0BCFF",
        contrastText: "#381E72",
      },
      name: "violet",
    }),
  },
});

const CutEditorActionBar = () => {
  const {
    currentTime,
    waveform,
    isPlay,
    setIsPlay,
    duration,
    setIsFillNewSpeechGrid,
    listCutEditor,
    setIsLoading,
    setListCutEditor,
    cutEditorFile
  } = appStore();
  const [search] = useSearchParams();
  const post_id = search.get("post_id");
  const [value, setValue] = React.useState(0);
  const [formattedCurrentTime, setFormattedCurrentTime] = React.useState(null);
  const [formattedDuration, setFormattedDuration] = React.useState(null);
  React.useEffect(() => {
    if (waveform) {
      if (isPlay) {
        waveform.play();
      } else {
        waveform.pause();
      }
    }
  }, [isPlay, waveform]);
  const handleChange = (event, newValue) => {
    setValue(newValue);
  };
  const handlePlay = () => {
    setIsPlay(!isPlay);
  };
  React.useEffect(() => {
    if (duration > 0) {
      if (waveform) {
        waveform.zoom(value);
      }
    }
  }, [value, waveform, duration]);

  const handleForwardSkip = () => {
    waveform.skip(10);
  };
  const handleBackwardSkip = () => {
    waveform.skip(-10);
  };

  useEffect(() => {
    setFormattedCurrentTime(formatToMinuteSecond(currentTime));
  }, [currentTime]);
  useEffect(() => {
    setFormattedDuration(formatToMinuteSecond(duration));
  }, [duration]);
  const handleExport = async () => {
    setIsLoading(true)
    try {
      await exportCutEditor(post_id, cutEditorFile, listCutEditor,false)
    } catch (ex) {
      alert(ex.message)
    }
    setIsLoading(false)
  };

  const handleCut = () => {
    setIsFillNewSpeechGrid(true);
  };
  return (
    <ThemeProvider theme={theme}>
      {" "}
      <div
        style={{
          display: "flex",
          flexDirection: "column",
          height: 80,
          backgroundColor: "#202027",
          color: "#E6E0E9",
          padding: "0px 15px",
          border: "1px solid #322F35",
          // flexWrap:'wrap'
          overflow:'auto'
        }}
      >
        <div
          style={{
            display: "flex",
            justifyContent: "space-between",
            alignItems: "center",
            height: "100%",

          }}
        >
          <div
            style={{
              display: "flex",
              minWidth: 200,
              gap: 5,
              alignItems: "center",
            }}
          >
            <ZoomOutIcon />
            <Slider
              aria-label="Volume"
              value={value}
              onChange={handleChange}
              color="violet"
            />
            <ZoomInIcon style={{ paddingLeft: 9 }} />
          </div>
          <div style={{ display: "flex", alignItems: "center", gap: 10 }}>
            <div>
              <div>{formattedCurrentTime}</div>
            </div>
            <div>
              <ButtonGroup size="large">
                <IconButton
                  aria-label="rewind"
                  sx={{ color: "#D0BCFF" }}
                  onClick={handleBackwardSkip}
                >
                  <FastRewindRoundedIcon style={{ fontSize: 35 }} />
                </IconButton>
                <IconButton
                  aria-label="skip"
                  sx={{ color: "#D0BCFF" }}
                  onClick={handlePlay}
                >
                  {!isPlay ? (
                    <PlayArrowRounded style={{ fontSize: 35 }} />
                  ) : (
                    <PauseRounded style={{ fontSize: 35 }} />
                  )}
                </IconButton>
                <IconButton
                  aria-label="forward"
                  sx={{ color: "#D0BCFF" }}
                  onClick={handleForwardSkip}
                >
                  <FastForwardRoundedIcon style={{ fontSize: 35 }} />
                </IconButton>
              </ButtonGroup>
            </div>
            <div>{formattedDuration}</div>
          </div>
          <div style={{ display: "flex", gap: 10 }}>
            <Button
              variant="contained"
              color="violet"
              style={{ borderRadius: 100 }}
              onClick={() => {
                if (!window.confirm('Are you sure?')) return
                let clone = []
                setListCutEditor(clone)
                createMarkup(window.wsRegions, clone);
              }}
            >
              Remove all selections
            </Button>
            <Button
              variant="contained"
              color="violet"
              style={{ borderRadius: 100 }}
              onClick={handleCut}
            >
              Cut
            </Button>
            <Button
              variant="contained"
              color="violet"
              style={{ borderRadius: 100 }}
              onClick={handleExport}
            >
              Save
            </Button>
          </div>
        </div>
      </div>
    </ThemeProvider>
  );
};

export default CutEditorActionBar;
