import * as React from "react";
import { TextField, createTheme } from "@mui/material";
import { ThemeProvider } from "@emotion/react";
import { appStore } from "../../store/app.store";

const theme = createTheme({
  palette: {
    gray: {
      main: "#938F99",
    },
  },
});

export default function Metadata() {
  const { transcriptFile, setTranscriptFile } = appStore();

  const handleChange = (type, e) => {
    debugger;
    let clone = { ...transcriptFile };
    clone.file[type] = e.target.value;
    setTranscriptFile(clone);
  };

  const handleChange1 = (type, e) => {
    debugger;
    let clone = { ...transcriptFile };
    clone.transcript[type] = e.target.value;
    setTranscriptFile(clone);
  };
  return (
    <ThemeProvider theme={theme}>
      <div
        style={{
          display: "flex",
          gap: 20,
          justifyContent: "space-between",
          flexDirection: "column",
        }}
      >
        <div>
          <TextField
            className="cba-text-field"
            fullWidth
            id="outlined-basic"
            label="File name"
            variant="outlined"
            size="large"
            sx={{
              "& .MuiInputBase-root": {
                color: "gray.main",
              },
            }}
            color="gray"
            focused
            value={transcriptFile?.file.title}
            onChange={handleChange.bind(this, "title")}
          />
        </div>
        <div>
          <TextField
            className="cba-text-field"
            fullWidth
            id="outlined-basic"
            label="Author"
            variant="outlined"
            size="large"
            sx={{
              "& .MuiInputBase-root": {
                color: "gray.main",
              },
            }}
            color="gray"
            focused
            value={transcriptFile?.transcript.author}
            onChange={handleChange1.bind(this, "author")}
          />
        </div>
        <div>
          <TextField
            className="cba-text-field"
            fullWidth
            id="outlined-basic"
            label="License"
            variant="outlined"
            size="large"
            sx={{
              "& .MuiInputBase-root": {
                color: "gray.main",
              },
            }}
            color="gray"
            focused
            value={
              transcriptFile?.transcript.license
                ? transcriptFile?.transcript.license
                : "cc by"
            }
            onChange={handleChange1.bind(this, "license")}
          />
        </div>
      </div>
    </ThemeProvider>
  );
}
