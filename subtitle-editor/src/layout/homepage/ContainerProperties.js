import * as React from "react";
import Typography from "@mui/material/Typography";
import PropTypes from "prop-types";
import Tabs from "@mui/material/Tabs";
import Tab from "@mui/material/Tab";
import Box from "@mui/material/Box";
import Properties from "./Properties";
import Metadata from "./Metadata";
import Stack from "@mui/material/Stack";
import image from "../../images/Bild1.png";
import { createTheme } from "@mui/material/styles";

let theme = createTheme({});
theme = createTheme(theme, {
  palette: {
    violet: theme.palette.augmentColor({
      color: {
        main: "#D0BCFF",
        contrastText: "#E6E0E9",
      },
      name: "violet",
    }),
  },
});

function CustomTabPanel(props) {
  const { children, value, index, ...other } = props;

  return (
    <div
      role="tabpanel"
      hidden={value !== index}
      id={`simple-tabpanel-${index}`}
      aria-labelledby={`simple-tab-${index}`}
      {...other}
    >
      {value === index && (
        <Box sx={{ p: 3 }}>
          <Typography>{children}</Typography>
        </Box>
      )}
    </div>
  );
}

CustomTabPanel.propTypes = {
  children: PropTypes.node,
  index: PropTypes.number.isRequired,
  value: PropTypes.number.isRequired,
};

function a11yProps(index) {
  return {
    id: `simple-tab-${index}`,
    "aria-controls": `simple-tabpanel-${index}`,
  };
}

export default function ContainerProperties() {
  const [value, setValue] = React.useState(0);

  const handleChange = (event, newValue) => {
    setValue(newValue);
  };

  return (
    <>
      <Box
        sx={{
          width: 500,
          height: "calc(100% - 0px)",
          borderRight: "1px solid #322F35",
        }}
      >
        <Stack style={{ height: "100%" }} spacing={1}>
          <div style={{ padding: 30, backgroundColor: "#1D1B20" }}>
            <img src={image} alt="image" width="420" height="220" />
          </div>

          <Box
            marginTop={"0px !important"}
            sx={{ bgcolor: "#1D1B20", width: "100%", height: "100%", overflow:'auto' }}
          >
            <Box sx={{ borderBottom: 1, borderColor: "divider" }}>
              <Tabs
                className="cba-tabs"
                value={value}
                onChange={handleChange}
                style={{ color: "purple !important"  }}
                variant="fullWidth"
              >
                <Tab label="Subtitles" {...a11yProps(0)} />
                <Tab label="Metadata" {...a11yProps(1)} />
              </Tabs>
            </Box>
            <CustomTabPanel value={value} index={0}>
              <Properties />
            </CustomTabPanel>
            <CustomTabPanel value={value} index={1}>
              <Metadata />
            </CustomTabPanel>
          </Box>
        </Stack>
      </Box>
    </>
  );
}
