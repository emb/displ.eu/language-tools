import * as React from "react";
import List from "@mui/material/List";
import ListItem from "@mui/material/ListItem";
import ListItemText from "@mui/material/ListItemText";
import ListItemButton from "@mui/material/ListItemButton";
import ListItemIcon from "@mui/material/ListItemIcon";
import Divider from "@mui/material/Divider";
import Typography from "@mui/material/Typography";
import { createTheme, ThemeProvider } from "@mui/material/styles";
import axios from "axios";
import _ from "lodash";
import { appStore } from "../../store/app.store";
import { useSearchParams } from "react-router-dom";
import DeleteIcon from '@mui/icons-material/Delete';
import { format, parse } from 'date-fns';
let theme = createTheme({});
theme = createTheme(theme, {
  palette: {
    violet: theme.palette.augmentColor({
      color: {
        main: "#D0BCFF",
        contrastText: "#CAC4D0",
      },
      name: "violet",
    }),
  },
});

const formatDate = (inputDate) => {
  const parsedDate = parse(inputDate, 'yyyy-MM-dd HH:mm:ss', new Date());
  return format(parsedDate, 'dd.MM.yy HH:mm');
};

export default function Properties() {
  const {
    setIsLoading,
    setTranslationContent,
    transcripts,
    translations, transcriptFile
  } = appStore();
  const [check, setCheck] = React.useState("");
  const [search] = useSearchParams();

  const handleCheck = async (item, e) => {

  };
  const handleDelete = async (item, e) => {
    e.stopPropagation()
    if (!window.confirm('Are you sure?')) {
      return
    }
    setIsLoading(true)
    try {
      var data = new URLSearchParams();
      data.append('action', 'trp_editor_delete_transcript');
      data.append('transcript_id', item.post_id);
      data.append("_ajax_nonce", transcriptFile.nonce)

      fetch("https://cba.media/wp-admin/admin-ajax.php", {
        method: "POST",
        body: data,
        headers: {
          "Content-type": "application/x-www-form-urlencoded; charset=UTF-8"
        }
      });
    } catch (ex) {
      alert(ex.message)
    }
    setIsLoading(false)
  }
  const handleSelectItem = async (item) => {
    setIsLoading(true);
    try {
      if (check == item.id) {
        setCheck("");
        setTranslationContent(null);
        setIsLoading(false);
        return;
      }
      let res = await axios.get(
        `https://cba.media/wp-admin/admin-ajax.php?transcript_id=${item.id}&action=trp_editor_load_transcript`
      );
      setTranslationContent(res.data.transcript);
      setCheck(item.id);

    } catch (ex) {
      alert(ex.message);
    }
    setIsLoading(false);
  }
  return (
    <>
      <ThemeProvider theme={theme}>
        <List
          sx={{
            width: "100%",
            maxWidth: 470,
            bgcolor: "#1D1B20",
            color: "#E6E0E9",
          }}
        >
          <ListItem disablePadding>
            <ListItemButton>
              <ListItemText primary="Original" />
            </ListItemButton>
          </ListItem>
          {transcripts.map((v) => (
            <ListItem style={{ marginLeft: 20 }} >
              <ListItemText
                primary={v.language_string}
                secondary={
                  <React.Fragment>
                    <Typography
                      sx={{ display: "inline" }}
                      component="span"
                      variant="body2"
                      color="#E6E0E9"
                    >
                      <div>
                        Created: {v.date_created}
                      </div>
                      <div>
                        Author:{" "}
                        {v.author === "" ? "Unknown author" : v.author}
                      </div>
                    </Typography>
                  </React.Fragment>
                }
              />

            </ListItem>
          ))}

          <Divider />

          <ListItem disablePadding>
            <ListItemButton>
              <ListItemText primary="Translations" />
            </ListItemButton>
          </ListItem>

          {translations.map((v) => (
            <ListItem onClick={handleSelectItem.bind(this, v)}
              className={`cba-list-translation ${v.id === check ? 'active' : ''}`} style={{ marginLeft: 20 }} >
              <ListItemText
                fontSize="50"
                primary={v.language_string}
                secondary={
                  <React.Fragment>
                    <Typography
                      sx={{ display: "inline" }}
                      component="span"
                      variant="body2"
                      color="#E6E0E9"
                    >
                      <div>
                      Created: {v.date_created}
                      </div>
                      <div>
                      Author:{" "}
                      {v.author === "" ? "Unknown author" : v.author}
                      </div>
                    </Typography>
                  </React.Fragment>
                }
              />
              <ListItemIcon>
                <div style={{ marginLeft: 50 }} onClick={handleDelete.bind(this, v)}>
                  <DeleteIcon sx={{ color: "#CAC4D0 !important", fontSize: 20 }}
                  />
                </div>

              </ListItemIcon>
            </ListItem>
          ))}
        </List>
      </ThemeProvider>
    </>
  );
}