import * as React from "react";
import { useEffect, useState } from "react";
import EditTranscript from "../../components/EditTranscript";
import _ from "lodash";
import { appStore } from "../../store/app.store";

const TranscriptSection = () => {
  const [combine, setCombine] = useState([]);
  const {
    translationContent,
    transcriptContent,
  } = appStore();

  useEffect(() => {
    if (transcriptContent) {
      let result = [];
      let mapping = new Map();
      for (let index = 0; index < transcriptContent.segments.length; index++) {
        const transcript = transcriptContent.segments[index];
        mapping.set(`${transcript.start}-${transcript.end}`, {
          id: transcript.id,
          start: transcript.start,
          end: transcript.end,
          transcript: transcript.text,
          translated: '',
        });
      }
      if (translationContent) {
        for (let index = 0; index < translationContent.segments.length; index++) {
          const translated = translationContent.segments[index]
          let key = `${translated.start}-${translated.end}`
          if (mapping.has(key)) {
            let value = mapping.get(key)
            value.translated = translated.text
          }
        }
      }
      let array = Array.from(mapping, ([name, value]) => (value));
      setCombine(array);
    }
  }, [translationContent, transcriptContent]);
  return (
    <>
      <div
        style={{
          display: "flex",
          height: "calc(100% - 460px)",
          padding: 40,
          overflow: "auto",
          backgroundColor: "#1D1B20",
          color: "white",
          justifyContent: translationContent === null ? 'center' : ''
        }}
      >
        <div style={{ display: "flex", flexDirection: "column", gap: 25, maxWidth: translationContent === null ?1000 :'unset'}}>
        {combine.map((item, k) => (
          <div
            style={{
              display: "flex",
              gap: 25,
              alignItems: "center",
              height: "100%",
            }}
          >
            <div
              style={{ width: translationContent === null ? "100%" : "50%" }}
            >
              <EditTranscript
                item={item}
                text={item.transcript}
                type={"transcript"}
              />
            </div>

            {translationContent && (
              <div style={{ width: "50%" }}>
                <EditTranscript
                  item={item}
                  text={item.translated}
                  type={"translated"}
                />
              </div>
            )}
          </div>
        ))}
      </div>
    </div >
    </>
  );
};

export default TranscriptSection;
