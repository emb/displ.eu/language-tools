import * as React from "react";
import { TextField, createTheme, Link, Button } from "@mui/material";
import { ThemeProvider } from "@emotion/react";
import { appStore } from "../../store/app.store";

const theme = createTheme({
  palette: {
    gray: {
      main: "#938F99",
    },
  },
});

export default function Metadata() {
  const { setCutEditorFile, cutEditorFile } = appStore();
  const handleChange = (type, e) => {
    debugger;
    let clone = { ...cutEditorFile };
    clone[type] = e.target.value;
    setCutEditorFile(clone);
  };
  const handleOpenLink = () => {
    window.open(cutEditorFile?.cut_url)
  }
  return (
    <ThemeProvider theme={theme}>
      <div
        style={{
          display: "flex",
          gap: 20,
          justifyContent: "space-between",
          flexDirection: "column",
        }}
      >
        {cutEditorFile?.cut_url ? (
          <div style={{ display: 'flex', gap: 2 }} >
            <TextField
              aria-readonly
              className="cba-text-field"
              fullWidth
              id="outlined-basic"
              label="Cut URL"
              variant="outlined"
              size="large"
              sx={{
                "& .MuiInputBase-root": {
                  color: "gray.main",
                },
              }}
              color="gray"
              focused
              value={cutEditorFile?.cut_url}
            />
            <Button onClick={handleOpenLink} variant="contained">Link</Button>
          </div>
        ) :
          <div  >
            <TextField
              aria-readonly
              className="cba-text-field"
              fullWidth
              id="outlined-basic"
              label="Cut URL"
              variant="outlined"
              size="large"
              sx={{
                "& .MuiInputBase-root": {
                  color: "gray.main",
                },
              }}
              color="gray"
              focused
              value={`No cut version available yet. Create selections and click on "Cut" to add one without copyrighted material.`}
            />
          </div>
        }
        <div>
          <TextField
            className="cba-text-field"
            fullWidth
            id="outlined-basic"
            label="File name"
            variant="outlined"
            size="large"
            sx={{
              "& .MuiInputBase-root": {
                color: "gray.main",
              },
            }}
            color="gray"
            value={cutEditorFile?.title}
            onChange={handleChange.bind(this, "title")}
            focused
          />
        </div>
        <div>
          <TextField
            className="cba-text-field"
            fullWidth
            id="outlined-basic"
            label="License"
            variant="outlined"
            size="large"
            sx={{
              "& .MuiInputBase-root": {
                color: "gray.main",
              },
            }}
            color="gray"
            focused
            value={cutEditorFile?.license ? cutEditorFile?.license : "cc by"}
            onChange={handleChange.bind(this, "license")}
          />
        </div>

      </div>
    </ThemeProvider>
  );
}
