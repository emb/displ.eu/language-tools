import { create } from "zustand";

const initialState = {
  transcriptContent: null,
  translationContent: null,
  currentTime: 0,
  waveform: null,
  isPlay: null,
  duration: 0,
  fileUrl: null,
  currentTranscriptSelected: null,
  isLoading: false,
  isSettingModal: false,
  isAddNewSpeechGrid: false,
  isFillNewSpeechGrid: false,
  newSpeechGrid:null,
  listCutEditor: [],
  fileUrlCutEditor: null,
  listFileCutEditor:[],
  transcriptFile: null,
  cutEditorFile: null,
  translations:[],
  transcripts:[],
  cutEditorRegionSelected :null
};
export const appStore = create((set) => ({
  ...initialState,
  setTranscriptContent: (value) =>
    set((state) => ({ transcriptContent: value })),
  setTranslationContent: (value) =>
    set((state) => ({ translationContent: value })),
  setCurrentTime: (value) => set((state) => ({ currentTime: value })),
  setWaveform: (value) => set((state) => ({ waveform: value })),
  setIsPlay: (value) => set((state) => ({ isPlay: value })),
  setDuration: (value) => set((state) => ({ duration: value })),
  setCurrentTranscriptSelected: (value) =>
    set((state) => ({ currentTranscriptSelected: value })),
  setFileUrl: (value) => set((state) => ({ fileUrl: value })),
  setIsLoading: (value) => set((state) => ({ isLoading: value })),
  setIsSettingModal: (value) => set((state) => ({ isSettingModal: value })),
  setIsFillNewSpeechGrid: (value) => set((state) => ({ isFillNewSpeechGrid: value })),
  setIsAddNewSpeechGrid: (value) => set((state) => ({ isAddNewSpeechGrid: value })),
  setNewSpeechGrid: (value) => set((state) => ({ newSpeechGrid: value })),
  setListCutEditor: (value) => set((state) => ({ listCutEditor: value })),
  setFileUrlCutEditor: (value) => set((state) => ({ fileUrlCutEditor: value })),
  setListFileCutEditor: (value) => set((state) => ({ listFileCutEditor: value })),
  setCutEditorFile: (value) => set((state) => ({ cutEditorFile: value })),
  setTranscriptFile: (value) => set((state) => ({ transcriptFile: value })),
  setTranslations: (value) => set((state) => ({ translations: value })),
  setTranscripts: (value) => set((state) => ({ transcripts: value })),
  setCutEditorRegionSelected: (value) => set((state) => ({ cutEditorRegionSelected: value })),
  resetAppStore: () => set(initialState),
}));
