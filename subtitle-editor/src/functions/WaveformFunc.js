import _ from "lodash";

export const createMarkup = (wsRegions, list) => {
  wsRegions.clearRegions();
  _.forEach(list, (v, k) => {
    let img = document.createElement("img");
    img.id = k;
    img.src =
      v.type === "music"
        ? "https://img.icons8.com/CC0000/material-outlined/24/apple-music.png"
        : "https://img.icons8.com/6AA84F/material-outlined/24/comments--v1.png";
    img.width = 24;
    img.height = 24;
    img.attributes.type = v.type
    wsRegions.addRegion({
      // channelIdx: k,
      id:
        v.type === "music"
          ? "cba-waveform-marker-music"
          : "cba-waveform-marker-spoken-word",
      start: v.from / 1000,
      end: v.to / 1000,
      content: img,
      resize: true,
      drag: false,
    });
  });
};

export const snapSpeechGrid = (current, next, previous, distance, factor = 1) => {
  //* The factor default is second
  let from = current.from;
  let to = current.to;
  if (previous) {
    if ((current.from - previous.to) <= distance * factor) {
      from = previous.to;
    }
  }
  if (next) {
    if ((next.from - current.to) <= distance * factor) {
      to = next.from;
    }
  }
  return { from, to }
}

export const exportCutEditor = async (post_id, cutEditorFile, listCutEditor, copyright ) => {
  return new Promise(async (resolve, reject) => {
    try {
      // if (listCutEditor.length === 0) {
      //   alert('Wavefom does not have any regions')
      //   return
      // }
      // if (listCutEditor.length === 1) {
      //   let current = listCutEditor[0];
      //   if (current.from >= 500) {
      //     isGap= true
      //   }
      //   if (duration*1000 - current.to >500) {
      //     isGap= true
      //   }

      // } else {
      //   for (let index = 0; index < listCutEditor.length; index++) {
      //     let current = listCutEditor[index];
      //     let next = null
      //     let previous = null
      //     if (index === 0) {
      //       next = listCutEditor[index + 1]
      //       if (current.from >= 500) {
      //         isGap= true
      //         break
      //       }
      //     } else if (index === listCutEditor.length - 1) {
      //       previous = listCutEditor[index - 1]
      //       if (duration*1000 - current.to >500) {
      //         isGap= true
      //         break
      //       }
      //     } else {
      //       next = listCutEditor[index + 1]
      //       previous = listCutEditor[index - 1]
      //     }
      //     if (previous) {
      //       if (current.from - previous.to !== 0) {
      //         isGap = true
      //         break
      //       }
      //     }
      //     if (next) {
      //       if (next.from - current.to !== 0) {
      //         isGap = true
      //         break
      //       }
      //     }
      //   }
      // }

      // if (isGap) {
      //   alert('You have to fill the gap')
      //   return
      // }


      var data = new URLSearchParams();
      data.append('action', 'trp_editor_save_regions');
      data.append('post_id', parseInt(post_id));
      data.append("title", cutEditorFile.title)
      data.append("license","" ) ///cutEditorFile.license
      data.append("remove_copyright", copyright)
      data.append('regions', JSON.stringify(listCutEditor));

      await fetch("https://cba.media/wp-admin/admin-ajax.php", {
        method: "POST",
        body: data,
        headers: {
          "Content-type": "application/x-www-form-urlencoded; charset=UTF-8"
        }
      });
      resolve()
    } catch (ex) {
      alert(ex.message)
      reject(ex)
    }
  })
};