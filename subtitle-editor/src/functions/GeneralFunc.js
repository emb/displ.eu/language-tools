export const formatToMinuteSecond = (totalSecond) => {
    const hours = Math.floor(totalSecond / 3600);
    const minutes = Math.floor((totalSecond % 3600) / 60);
    const seconds = Math.floor((totalSecond % 3600) % 60);
    const formattedHours = hours < 10 ? `0${hours}` : hours;
    const formattedMinutes = minutes < 10 ? `0${minutes}` : minutes;
    const formattedSeconds = seconds < 10 ? `0${seconds}` : seconds;
    const formattedTime = `${formattedHours}:${formattedMinutes}:${formattedSeconds}s`;
    return formattedTime;
  };

  export const sleep = (ms) => {
    return new Promise((resolve) => setTimeout(resolve, ms));
  };