# Getting Started with Create React App

This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

## Available Scripts

In the project directory, you can run:

### `npm start`

Runs the app in the development mode.\
Open [http://localhost:3000](http://localhost:3000) to view it in your browser.

The page will reload when you make changes.\
You may also see any lint errors in the console.

### `npm test`

Launches the test runner in the interactive watch mode.\
See the section about [running tests](https://facebook.github.io/create-react-app/docs/running-tests) for more information.

### `npm run build`

Builds the app for production to the `build` folder.\
It correctly bundles React in production mode and optimizes the build for the best performance.

Before you run this command, make sure to include the full relative server path of the editor in the <BrowserRouter>'s `basename` attribute in `src/index.js`:

```
const root = ReactDOM.createRoot(document.getElementById("root"));
root.render(
  <BrowserRouter basename="/wp-content/plugins/transposer/js/transcript-editor/build/index.html">
    <App />
  </BrowserRouter>
);
```

The build is minified and the filenames include the hashes.\
Your app is ready to be deployed!

See the section about [deployment](https://facebook.github.io/create-react-app/docs/deployment) for more information.

### `npm run eject`

**Note: this is a one-way operation. Once you `eject`, you can't go back!**

If you aren't satisfied with the build tool and configuration choices, you can `eject` at any time. This command will remove the single build dependency from your project.

Instead, it will copy all the configuration files and the transitive dependencies (webpack, Babel, ESLint, etc) right into your project so you have full control over them. All of the commands except `eject` will still work, but they will point to the copied scripts so you can tweak them. At this point you're on your own.

You don't have to ever use `eject`. The curated feature set is suitable for small and middle deployments, and you shouldn't feel obligated to use this feature. However we understand that this tool wouldn't be useful if you couldn't customize it when you are ready for it.

## Learn More

You can learn more in the [Create React App documentation](https://facebook.github.io/create-react-app/docs/getting-started).

To learn React, check out the [React documentation](https://reactjs.org/).

### Code Splitting

This section has moved here: [https://facebook.github.io/create-react-app/docs/code-splitting](https://facebook.github.io/create-react-app/docs/code-splitting)

### Analyzing the Bundle Size

This section has moved here: [https://facebook.github.io/create-react-app/docs/analyzing-the-bundle-size](https://facebook.github.io/create-react-app/docs/analyzing-the-bundle-size)

### Making a Progressive Web App

This section has moved here: [https://facebook.github.io/create-react-app/docs/making-a-progressive-web-app](https://facebook.github.io/create-react-app/docs/making-a-progressive-web-app)

### Advanced Configuration

This section has moved here: [https://facebook.github.io/create-react-app/docs/advanced-configuration](https://facebook.github.io/create-react-app/docs/advanced-configuration)

### Deployment

This section has moved here: [https://facebook.github.io/create-react-app/docs/deployment](https://facebook.github.io/create-react-app/docs/deployment)

### `npm run build` fails to minify

This section has moved here: [https://facebook.github.io/create-react-app/docs/troubleshooting#npm-run-build-fails-to-minify](https://facebook.github.io/create-react-app/docs/troubleshooting#npm-run-build-fails-to-minify)




# Transcript Editor API Specifications

## TODO

- Video support (playback the video when editing)
- Add configuration file and let load and save endpoints be customized
- Add i18n support (interface translations)


## Load endpoint

To load a transcript into the editor you need to provide 3 things:

- Metadata for the file that is being edited
- Metadata for the transcript that is being edited
- A list of existing translations of the original transcript

These informations are contained in 3 objects: file, transcript and transcripts.

TODO: The URL of the load endpoint needs to be defined in the config file.

To open the editor and load the transcript, call the build's index.html with the GET parameter `?transcript_id=`:

`/transcript-editor/build/static/index.html?transcript_id=<transcript_id>`

The editor will then call the load endpoint with this GET parameter as well, e.g.

`https://my.url.com/my-load-endpoint/?transcript_id=<transcript_id>`

```JSON
{
  "action": "trp_editor_load_transcript",
  "file": {
    "id": 504727,
    "title": "20210705_anno_von Schl\\u00e4gl bis Kneipp",
    "user_id": "5313",
    "date_created": "2021-06-15 08:40:04",
    "date_modified": "2021-06-15 11:37:44",
    "url": "https:\\/\\/cba.media\\/wp-content\\/uploads\\/7\\/2\\/0000504727\\/20210705-anno-von-schlaegl-bis-kneipp.mp3",
    "cut_url": "",
    "fileformat": "mp3",
    "bitrate": 192000,
    "mime_type": "audio\\/mpeg",
    "length": 3598,
    "length_formatted": "59:58",
    "license": "cc by-nc 4.0"
  },
  "transcript": {
    "id": "27223",
    "post_id": "504727",
    "model": "medium",
    "language": "de",
    "type": "auto",
    "segments": [
      {
        "id": 0,
        "start": 6.3,
        "end": 10.36,
        "text": " Anno Geschichte kann begeistern",
        "words": [
          {
            "text": "Anno",
            "start": 6.3,
            "end": 6.98,
            "confidence": 0.43
          },
          {
            "text": "Geschichte",
            "start": 6.98,
            "end": 8.64,
            "confidence": 0.481
          },
          {
            "text": "kann",
            "start": 8.64,
            "end": 9.36,
            "confidence": 0.968
          },
          {
            "text": "begeistern",
            "start": 9.36,
            "end": 10.36,
            "confidence": 0.996
          }
        ]
      },
      {
        "id": 1,
        "start": 10.92,
        "end": 16.32,
        "text": " Die monatliche Sendung mit Peter Saplatnik im Freien Radio Freistadt",
        "words": [
          {
            "text": "Die",
            "start": 10.92,
            "end": 11.5,
            "confidence": 0.851
          },
          {
            "text": "monatliche",
            "start": 11.5,
            "end": 12.28,
            "confidence": 0.962
          },
          {
            "text": "Sendung",
            "start": 12.28,
            "end": 12.84,
            "confidence": 0.994
          },
          {
            "text": "mit",
            "start": 12.84,
            "end": 13.24,
            "confidence": 0.983
          },
          {
            "text": "Peter",
            "start": 13.24,
            "end": 13.76,
            "confidence": 0.924
          },
          {
            "text": "Saplatnik",
            "start": 13.76,
            "end": 14.48,
            "confidence": 0.484
          },
          {
            "text": "im",
            "start": 14.48,
            "end": 15.04,
            "confidence": 0.945
          },
          {
            "text": "Freien",
            "start": 15.04,
            "end": 15.36,
            "confidence": 0.691
          },
          {
            "text": "Radio",
            "start": 15.36,
            "end": 15.7,
            "confidence": 0.946
          },
          {
            "text": "Freistadt",
            "start": 15.7,
            "end": 16.32,
            "confidence": 0.858
          }
        ]
      }

      ...

    ],
    "parent_id": "0",
    "date_created": "2023-12-05 13:47:59",
    "date_modified": "2023-12-05 13:47:59",
    "author": "Max Musterfrau",
    "license": "by 4.0",
    "user_id": "0",
    "language_string": "German",
    "subtitles": "https:\\/\\/cba.media\\/wp-content\\/uploads\\/7\\/2\\/0000504727\\/20210705-anno-von-schlaegl-bis-kneipp_de.vtt"
  },
  "transcripts": [
    {
      "id": "27223",
      "post_id": "504727",
      "model": "medium",
      "duration": "898",
      "language": "de",
      "type": "auto",
      "parent_id": "0",
      "date_created": "2023-12-05 13:47:59",
      "date_modified": "2023-12-05 13:47:59",
      "author": "",
      "license": "",
      "user_id": "0",
      "subtitles": "https:\\/\\/cba.media\\/wp-content\\/uploads\\/7\\/2\\/0000504727\\/20210705-anno-von-schlaegl-bis-kneipp_de.vtt",
      "language_string": "German"
    }
  ],
  "nonce": "f3a197b9ad"
}
```

### The action key

| Key | Data Type | Description |
| ------ | ------ | ------ |
| action | string | The action is optional. It is however mandatory for Wordpress to trigger the right saving function. Its value will always be 'trp_editor_load_transcript'.


### The file object

Contains metadata of the media file whose transcript is being edited.

| Key | Data Type | Description |
| ------ | ------ | ------ |
| id | int | The media file's ID
| title | string | The file's title
| date_created | datetime | The datetime the file was uploaded to the server (YYYY-MM-DD HH:MM:SS)
| date_modified | datetime | Last modification datetime of the file's description (YYYY-MM-DD HH:MM:SS)
| url | string | A URL to the media file (mp3, ogg, mp4)
| fileformat | string | Media's human readable file format (e.g. 'mp3', 'ogg', 'mp4')
| bitrate | int | The file's bitrate in bytes per second (bps)
| mime_type | string | The file's mime type (e.g. 'audio/mpeg', 'audio/ogg', 'video/mp4')
| length | int | The file's duration in seconds
| length_formatted | string | The file's duration in a human readable format, e.g. 'hh:mm:ss'
| license | string | The media file's license (e.h. 'cc by 4.0 de')


### The transcript object

Contains metadata of the transcript that should be edited and loaded into the editor.

| Key | Data Type | Description |
| ------ | ------ | ------ |
| id | int | The transcript's ID
| post_id | int | The media file the transcript relates to (is the same as in file.id)
| parent_id | int | The ID of the original transcript that this transcript/translation relates to. If 0 it means this transcript is the original. If >0 it means it is the translation of another transcript
| model | string | The language model the transcript was created with (e.g. 'whisper v20231117 medium', 'vosk-model-de-0.21', etc.). Just a descriptive information.
| language | string | The transcript's language in a 2 character [ISO-639-1 language code](https://en.wikipedia.org/wiki/List_of_ISO_639-1_codes) (e.g. 'en')
| language_string | string | The language in full words for displaying (e.g. 'Chinese')
| type | string | Indicate whether the transcript was created automatically or manually (either 'auto' or 'manual')
| date_created | datetime | The datetime the transcript was created (YYYY-MM-DD HH:MM:SS)
| date_modified | datetime | The datetime when the transcript was last modified  (YYYY-MM-DD HH:MM:SS)
| author | string | The transcript's author if available (can be empty)
| license | string | The trancript's license if available (can be empty. If empty it automatically means "All rights reserved" legally)
| subtitles | string | A URL to a WebVTT subtitle file (e.g. 'https://myurl.com/my-subtitles.vtt')
| segments | array of objects | An array of objects, containing the segments returned by Whisper. More info below.

#### The transcript.segments array

An array of objects containing the segments returned by Whisper.

Each object consists of:

| Key | Data Type | Description |
| ------ | ------ | ------ |
| id | int | An enumerated id of the segment, beginning at 0 |
| start | float | Segment's start in float seconds |
| end | float | Segment's end in float seconds |
| text | string | The segment's plain text that is displayed as subtitles between start and end
| words | array of objects | A segment can contain additional information about each word. This information is returned by whisper-timestamped and should be kept if it's there for later use

## The transcripts array

The array of objects contains a list of existing translations of the transcript that can be edited by the user.
It must also contain the transcript that is currently being edited.

Each object consists of:

| Key | Data Type | Description |
| ------ | ------ | ------ |
| id | int | The transcript's ID
| post_id | int | The media file the transcript relates to (is the same as in file.id)
| parent_id | int | The ID of the original transcript that this transcript/translation relates to. If 0 it means this transcript is the original. If >0 it means it is the translation of another transcript
| model | string | The language model the transcript was created with (e.g. 'whisper v20231117 medium', 'vosk-model-de-0.21', etc.). Just a descriptive information.
| language | string | The transcript's language in a 2 character [ISO-639-1 language code](https://en.wikipedia.org/wiki/List_of_ISO_639-1_codes) (e.g. 'en')
| language_string | string | The language in full words for displaying (e.g. 'Chinese')
| type | string | Indicate whether the transcript was created automatically or manually (either 'auto' or 'manual')
| date_created | datetime | The datetime the transcript was created (YYYY-MM-DD HH:MM:SS)
| date_modified | datetime | The datetime when the transcript was last modified  (YYYY-MM-DD HH:MM:SS)
| author | string | The transcript's author if available (can be empty)
| license | string | The trancript's license if available (can be empty. If empty it automatically means "All rights reserved" legally)
| subtitles | string | A URL to a WebVTT subtitle file (e.g. 'https://myurl.com/my-subtitles.vtt')


## Save endpoint

TODO: The URL of the save endpoint needs to be defined in the config file.

When pressing "save", the transcript's JSON data are sent as POST data in format `application/x-www-form-urlencoded` to the save endpoint.

### The save object

The object sent to the save endpoint needs to contain at least following data:

```JSON
{
    "action": "trp_editor_save_transcript",
    "transcript_id": 38040,
    "language": "de",
    "transcript": "Geschichte kann begeistern. Die monatliche Sendung mit Peter Saplatnik im Freien Radio Freistadt",
    "segments": [
      {
        "id": 0,
        "start": 6.3,
        "end": 10.36,
        "text": "Geschichte kann begeistern.",
      },
      {
        "id": 1,
        "start": 10.92,
        "end": 16.32,
        "text": " Die monatliche Sendung mit Peter S. im Freien Radio Freistadt",
      }

      ...

    ],
    "license": "cc by 4.0",
    "author": "Max Musterfrau",
    "_ajax_nonce": "dc018485db"
}
```

| Key | Data Type | Description |
| ------ | ------ | ------ |
| action | string | The action is optional. It is however mandatory for Wordpress to trigger the right saving function. Its value will always be 'trp_editor_save_transcript'.
| transcript_id | int | The ID of the transcript that is being saved
| language | string | The transcript's language in a 2 character [ISO-639-1 language code](https://en.wikipedia.org/wiki/List_of_ISO_639-1_codes) (e.g. 'en')
| transcript | string | The transcript's whole text as plain text (all segments' texts concatenated in one string)
| author | string | The transcript's author if available (can be empty)
| license | string | The trancript's license if available (can be empty. If empty it automatically means "All rights reserved" legally)
| segments | array of objects | The edited segment's in the same format as in the transcript.segments object
| _ajax_nonce | string | This is a hash that is used by Wordpress to check whether the call comes from the same server. In another system it is optional or can contain anything you'd like to match.