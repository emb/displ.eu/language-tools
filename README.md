# Description
One core goal of the project is, to provide content in the languages spoken in the countries of Europe. This drastically lowers the bar for consumption for the users and widens the space of delivery for producers.

To achieve this goal, the project utilizes state-of-the-art machine-transcription and -translation, to build the multilingual foundation. Editing tools allow human revisions in every step, ensuring the highest quality content possible, without road-blocking the release of content due to lack of initally human-provided translations.
