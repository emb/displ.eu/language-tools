# Workpackage/Task

## DISPL.EU 2023

* *Name:* **Subtitling & Translation**
* *Task-ID:* **T2.3**
* *Participants:*
  * **fairkom**
  * **CBA**
* *ERP:* **[TASK00165](https://erp.fairkom.net/app/task/TASK00165)**
